<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">


    <div class="card">
    <div align="center"> <img align="center" src="{{asset('images/Logo.png') }} "alt="" width="19040px" height="100px"></div>
  <h5 align="center"  class="card-header">Editar datos de perfil</h5>
 
  <div class="card-body">
  
<form action="{{ route('usuario.update',auth()->id() ) }}" method="POST">
    @csrf
    @method('PUT')
    <div class="row">
        <div  class="form-group col-md-6">
            <div class="form-group">
                <strong>Nombre:</strong>
                <input type="text" name="name" class="form-control" placeholder="Nombre">
            </div>
        </div>
        <div  class="form-group col-md-6">
            <div class="form-group">
                <strong>Correo Electronico:</strong>
                <input type="text" name="email" class="form-control" placeholder="Correo Electronico">
            </div>
        </div>
        <div  class="form-group col-md-6">
            <div class="form-group">
                <strong>Contraseña:</strong>
                <input type="text" name="password" class="form-control" placeholder="Contraseña">
            </div>
        </div>
        <div  class="form-group col-md-6">
            <div class="form-group">
                <strong>Division:</strong>
                <select class="custom-select" id="inputGroupSelect01"  name="division">
                    <option value="Económico-Administrativa">Económico-Administrativa</option>
                    <option value="Gastronomia">Gastronomia</option>
                    <option value="Turismo">Turismo</option>
                    <option value="Ingeniería y Tecnología">Ingeniería y Tecnología</option>
                    </select>
            </div>
        </div>
        <div  class="form-group col-md-6">
            <div class="form-group">
                <strong>Programa Educativo:</strong>
                <select class="custom-select" id="inputGroupSelect01"  name="programa">
                    <option value="TSU en Administración área Capital Humano">TSU en Administración área Capital Humano </option>
                    <option value="TSU en Contaduría">TSU en Contaduría</option>
                    <option value="TSU en Desarrollo de Negocios área Mercadotecnia">TSU en Desarrollo de Negocios área Mercadotecnia</option>
                    <option value="TSU en Gastronomía">TSU en Gastronomía</option>
                    <option value="TSU en Desarrollo de Software Multiplataforma">TSU en Desarrollo de Software Multiplataforma</option>
                    <option value="TSU en Mantenimiento área Instalaciones">TSU en Mantenimiento área Instalaciones</option>
                    <option value="TSU en Infraestructura de Redes Digitales">TSU en Infraestructura de Redes Digitales</option>
                    <option value="TSU en Hotelería">TSU en Hotelería</option>
                    <option value="TSU en Desarrollo de Productos Alternativos">TSU en Desarrollo de Productos Alternativos</option>
                    <option value="TSU en Terapia Física">TSU en Terapia Física</option>
                    <option value="Licenciatura en Gestión del Capital Humano">Licenciatura en Gestión del Capital Humano</option>
                    <option value="Licenciatura en Innovación de Negocios y Mercadotecnia">Licenciatura en Innovación de Negocios y Mercadotecnia</option>
                    <option value="Ingeniería Financiera y Fiscal">Ingeniería Financiera y Fiscal</option>
                    <option value="Licenciatura en Gastronomía">Licenciatura en Gastronomía</option> 
                    <option value="Ingeniería en Mantenimiento Industrial">Ingeniería en Mantenimiento Industrial</option>
                    <option value="Ingeniería en Tecnologías de la Información y Comunicación">Ingeniería en Tecnologías de la Información y Comunicación</option>
                    <option value="Licenciatura en Gestión y Desarrollo Turístico">Licenciatura en Gestión y Desarrollo Turístico</option>      
                </select>
            </div>
        </div>
        <div  class="form-group col-md-6">
            <div class="form-group">
                <strong>Nivel en el SEI:</strong>
                <select class="custom-select" id="inputGroupSelect01"  name="sei">
                    <option value="Si">Si</option>
                    <option value="No">No</option>
                    </select>
            </div>
        </div>
        <div  class="form-group col-md-6">
            <div class="form-group">
                <strong>PRODEP:</strong>
                <select class="custom-select" id="inputGroupSelect01"  name="prodep">
                    <option value="Si">Si</option>
                    <option value="No">No</option>
                    </select>
            </div>
        </div>
        <div  class="form-group col-md-6">
            <div class="form-group">
                <strong>Grado de Estudios:</strong>
                <select class="custom-select" id="inputGroupSelect01"  name="grado">
                    <option value="Licenciatura">Licenciatura</option>
                    <option value="Maestría">Maestría</option>
                    <option value="Licenciatura">Especialidad</option>
                    <option value="Maestría">Doctorado</option>
                    </select>
            </div>
        </div>
        <div  class="form-group col-md-6">
            <div class="form-group">
                <strong>Curp:</strong>
                <input type="text" name="curp" class="form-control" placeholder="Curp">
            </div>
        </div>
        <div  class="form-group col-md-6">
            <div class="form-group">
                <strong>RFC:</strong>
                <input type="text" name="rfc" class="form-control" placeholder="RFC">
            </div>
        </div>
        <div  class="form-group col-md-6">
            <div class="form-group">
                <strong>Cuerpo Académico:</strong>
                <select class="custom-select" id="inputGroupSelect01" name="cat_profesor">
                    <option value="PTC">PTC</option>
                    <option value="Coordinador de CAA">Coordinador de CAA</option>
                    <option value="Coordinador de División">Coordinador de División</option>
                    <option value="Jefe de departamento de Investigación">Jefe del departamento de Investigación</option>
                    <option value="Director de División">Director de División</option>
                    </select>
            </div>
        </div>
        <div class="col-auto my-3">
                <button type="submit" class="btn btn-primary btn-lg active">Enviar</button>
        </div>
    </div>


</form>

<footer class="footer" >
				<div class="container-fluid">
					<nav class="pull-left">
						<div class="copyright ml-auto">
							<b>Copyright</b> Universidad Tecnológica de Cancún @ 2020
						</div>	
					</nav>		
				</div>
			</footer>

  </div>
</div>














