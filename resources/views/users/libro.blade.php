@extends('layouts.app')

@section('Titulo', 'Repositorio de investigaciones ')

@section('content')


<!-- El apartado de formulario para poder crear el documento de tutoria  -->
<div class="panel-header colorut" >
		<div class="page-inner py-5">
			<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
			    <div>
			        <h2 class="text-black pb-2 fw-bold">Captura de Expedientes</h2>
		        </div>		
	        </div>
		</div>
    </div>
	<div class="page-inner mt--5">					
	    <!-- Contenido de Captura de expedientes-->
	    <!-- Cuadros de menu -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Captura de libro</div>
                    </div>
                <div class="card-body">
                    <form>
                        <div class="form-row">
                            <div class="form-group col-md-6 ">
                                <label for="inputEmail4">Contenido de libro</label>
                                <input type="text" class="form-control border border-secondary" id="inputEmail4" placeholder="Titulo">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Tapa de libro</label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Asunto">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Indice</label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Asunto">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Introducion</label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Asunto">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Divicion de partes</label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Asunto">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">capitulos</label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Asunto">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Concluciones</label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Asunto">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Recomendaciones</label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Asunto">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Bibliografia</label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Asunto">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Anexos</label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Asunto">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputCity">Fecha de libro </label>
                                <input type="date" class="form-control border border-secondary" id="inputDate" >
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputState">Carrera</label>
                                <select id="inputState" class="form-control border border-secondary">
                                    <option selected>Divsion de ingenieria</option>
                                    <option>TSU en Administración área Capital Humano</option>
                                    <option>TSU en Contaduría</option>
                                    <option>TSU en Desarrollo de Negocios área Mercadotecnia</option>
                                    <option>TSU en Gastronomía</option>
                                    <option>TSU en Tecnologías de la Información</option>
                                    <option>TSU en Turismo</option>
                                    <option>TSU en Terapia Física</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputState">Ingenieria</label>
                                <select id="inputState" class="form-control border border-secondary">
                                    <option>Licenciatura en Gestión del Capital Humano</option>
                                    <option>Licenciatura en Innovación de Negocios y Mercadotecnia</option>
                                    <option>Ingeniería Financiera y Fiscal</option>
                                    <option>Licenciatura en Gastronomía</option>
                                    <option>Ingeniería en Mantenimiento Industrial</option>
                                    <option>Ingeniería en Tecnologías de la Información y Comunicación</option>
                                    <option>Licenciatura en Gestión y Desarrollo Turístico</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Nombre del que presenta el libro</label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Asunto">
                            </div>
                            <div class="form-group col-md-2">
                                <label for="inputState">Captura de evidencia</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="validatedCustomFile" required>
                                    <label class="custom-file-label  border border-secondary" for="validatedCustomFile">Archivo..</label>
                                </div>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary">Subir Archivo</button>
                        </form>                                       
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Fin de apartado de formulario -->
@endsection
