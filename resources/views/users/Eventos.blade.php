@extends('layouts.app')

@section('Titulo', 'Repositorio de investigaciones ')

@section('content')
<!--  Este del formulario inicio de  gestion academida -->
<div class="panel-header colorut" >
		<div class="page-inner py-5">
			<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
			    <div>
			        <h2 class="text-black pb-2 fw-bold">Captura de Expedientes</h2>
		        </div>		
	        </div>
		</div>
    </div>
	<div class="page-inner mt--5">					
	    <!-- Contenido de Captura de expedientes-->
	    <!-- Cuadros de menu -->  
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Captura de Eventos</div>
                    </div>
                <div class="card-body">
                    <form>
                        <div class="form-row">
                            <div class="form-group col-md-6 ">
                                <label for="inputEmail4">Nombre de Ponencia</label>
                                <input type="text" class="form-control border border-secondary" id="inputEmail4" placeholder="Titulo">
                            </div>
                            <div class="form-group col-md-6 ">
                                <label for="inputEmail4">Nombre de taller</label>
                                <input type="text" class="form-control border border-secondary" id="inputEmail4" placeholder="Titulo">
                            </div>
                            <div class="form-group col-md-6 ">
                                <label for="inputEmail4">Nombre de congreso</label>
                                <input type="text" class="form-control border border-secondary" id="inputEmail4" placeholder="Titulo">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputState">Area de Entrega</label>
                                <select id="inputState" class="form-control border border-secondary">
                                    <option selected>Divsion de ingenieria</option>
                                    <option>Turismo</option>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="inputState">Captura de evidencia</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="validatedCustomFile" required>
                                    <label class="custom-file-label  border border-secondary" for="validatedCustomFile">Archivo..</label>
                                </div>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary">Subir Archivo</button>
                        </form>                                       
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Fin de formulario de gestion academida -->
@endsection
