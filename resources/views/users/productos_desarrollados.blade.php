@extends('layouts.app')

@section('Titulo', 'Repositorio de investigaciones ')

@section('content')
<!--  Este del formulario inicio de  gestion academida -->
<div class="panel-header colorut" >
		<div class="page-inner py-5">
			<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
			    <div>
			        <h2 class="text-black pb-2 fw-bold">Captura de Expedientes</h2>
		        </div>		
	        </div>
		</div>
    </div>
	<div class="page-inner mt--5">					
<!-- Contenido de Captura de expedientes-->
<!-- Cuadros de menu -->  
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Captura de Producto desarrollado</div>
                    </div>
                <div class="card-body">
                    <form action="{{ url('pdf_download_productosD') }}" method="post" accept-charset="utf-8">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6 ">
                                <label for="inputEmail4">Nombre del articulo</label>
                                <input type="text" class="form-control border border-secondary"  id="NombreArticulo" name="NombreArticulo" placeholder="Nombre del Articulo">
                                <span class="text-danger">{{ $errors->first('NombreArticulo') }}</span>
                            </div>
                            <div class="form-group col-md-6 ">
                                <label for="inputEmail4">Informe técnico</label>
                                <input type="text" class="form-control border border-secondary"   id="NombreInforme" name="NombreInforme" placeholder="Nombre del Informe tecnico">
                                <span class="text-danger">{{ $errors->first('NombreInforme') }}</span>
                            </div>
                            <div class="form-group col-md-6 ">
                                <label for="inputEmail4">Proyecto</label>
                                <input type="text" class="form-control border border-secondary"   id="Proyecto" name="Proyecto" placeholder="Titulo del proyecto">
                                <span class="text-danger">{{ $errors->first('Proyecto') }}</span>
                            </div>
                            <div class="form-group col-md-6 ">
                                <label for="inputEmail4">Patente</label>
                                <input type="text" class="form-control border border-secondary"   id="Patente" name="Patente" placeholder="Nombre de la Patente">
                                <span class="text-danger">{{ $errors->first('Patente') }}</span>
                            </div>
                            <div class="form-group col-md-6 ">
                                <label for="inputEmail4">Libro</label>
                                <input type="text" class="form-control border border-secondary"   id="Libro" name="Libro" placeholder="Nombre del libro">
                                <span class="text-danger">{{ $errors->first('Libro') }}</span>
                            </div>
                            <div class="form-group col-md-6 ">
                                <label for="inputEmail4">capítulo de libro</label>
                                <input type="text" class="form-control border border-secondary"   id="Capitulo" name="Capitulo" placeholder="Nombre del Capitulo del Libro">
                                <span class="text-danger">{{ $errors->first('Capitulo') }}</span>
                            </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputCity">fecha de desarrollo</label>
                                <input type="date" class="form-control border border-secondary"   id="Date" name="Date" >
                                <span class="text-danger">{{ $errors->first('Date') }}</span>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputState">Area de Entrega</label>
                                <select id="inputState" class="form-control border border-secondary"   id="Area" name="Area">
                                    <option selected>Divsion de ingenieria</option>
                                    <option>Turismo</option>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="inputState">Captura de evidencia</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="validatedCustomFile" >
                                    <label class="custom-file-label  border border-secondary" for="validatedCustomFile">Archivo..</label>
                                </div>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary">Generar Archivo</button>
                        </form>                                       
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Fin de formulario de gestion academida -->
@endsection
