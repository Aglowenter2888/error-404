@extends('layouts.app')
<style>
    .card-default {
    background: #23c6c8 !important;
}
</style>
@section('Titulo', 'Repositorio de investigaciones ')

@section('content')
    <div class="panel-header" style="background: #69bb85;">
        <div class="page-inner py-5">
            <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                <div>
                    <h2 class="text-white pb-2 fw-bold">Dashboard</h2>
                    <h5 class="text-white op-7 mb-2">Premium Bootstrap 4 Admin Dashboard</h5>
                </div>
                <div class="ml-md-auto py-2 py-md-0">
                    <a href="#" class="btn btn-white btn-border btn-round mr-2">Manage</a>
                    <a href="#" class="btn btn-secondary btn-round">Add Customer</a>
                </div>
            </div>
        </div>
    </div>
    <div class="page-inner mt--5">
    
    <!-- Contenido del Dashboard -->
        <div class="row">
            <!-- Cuadros de menu -->
            <div class="col-sm-6 col-lg-3">
                <div class="card p-3 bg-info text-white ">
                    <div class="card-body p-3 text-center">
                        <div class="h1 m-0">
                            <!-- Icono -->
                            <i class="la flaticon-file"></i>
                        </div>
                        <div class="mb-3 text-white ">Informes <br>realizados</div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card p-3 text-white" style="background: #69bb85;">
                    <div class="card-body p-3 text-center text-white ">
                        <div class="h1 m-0">
                            <!-- Icono -->
                            <i class="la flaticon-folder"></i>
                        </div>
                        <div class="text-white mb-3">Numero de <br>expediente</div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card p-3 bg-info">
                    <div class="card-body p-3 text-white  text-center">
                        <div class="h1 m-0">
                            <!-- Icono -->
                            <i class="la flaticon-users"></i>
                        </div>
                        <div class="text-white mb-3">Tutorias <br>Activas</div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card p-3" style="background: #69bb85;">
                    <div class="card-body p-3 text-center text-white ">
                        <div class="h1 m-0">
                            <!-- Icono -->
                            <i class="la flaticon-paypal"></i>
                        </div>
                        <div class="text-white mb-3">Productos <br>desarrollados</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Integracion de expedientes</div>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-right p-3">
                            <div id="barChart"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


<!-- @section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection -->
