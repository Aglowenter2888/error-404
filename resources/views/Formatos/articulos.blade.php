@extends('layouts.app')

@section('Titulo', 'Repositorio de investigaciones ')

@section('content')


<!-- El apartado de formulario para poder crear el documento de tutoria  -->
<div class="panel-header colorut" >
		<div class="page-inner py-5">
			<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
			    <div>
			        <h2 class="text-black pb-2 fw-bold">Producción Académica</h2>
		        </div>		
	        </div>
		</div>
    </div>
	<div class="page-inner mt--5">					
	    <!-- Contenido de Captura de expedientes-->
	    <!-- Cuadros de menu -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Artículo en revista</div>
                    </div>
                <div class="card-body">
                    <form>
                        <div class="form-row">
                            <div class="form-group col-md-6 ">
                                <label for="inputEmail4">Autor(es) </label>
                                <input type="text" class="form-control border border-secondary" id="inputEmail4" placeholder="Nombre del Autor">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Título </label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Nombre del Titulo">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Descripción </label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Escribir Estado">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Estado actual  </label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Escribir Pais">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">País  </label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Escribir Nombre">
                            </div>  
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Nombre de la Revista</label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Escribir Nombre">
                            </div> 
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Editorial  </label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Escribir Nombre">
                            </div> 
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">De la página (Numero de pagina)</label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Escribir Nombre">
                            </div> 
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">A la página (Numero de pagina)  </label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Escribir Nombre">
                            </div> 
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Volumen </label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Escribir Nombre">
                            </div> 
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Índice de registro de la revista </label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Escribir Nombre">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">ISSN </label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Escribir Nombre">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Dirección electrónica del artículo </label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Escribir Nombre">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Propósito  </label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Escribir Nombre">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Miembros </label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Escribir Nombre">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">LGACs </label>
                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Escribir Nombre">
                            </div>
                            <div class="form-group col-md-6">
                            <label for="inputPassword4">Para considerar en el currículum de cuerpo académico </label>
                                <select class="custom-select" id="inputGroupSelect01"  name="sei">
                                    <option value="Si">Si</option>
                                    <option value="No">No</option>
                                    </select>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputCity">Año y fecha </label>
                                <input type="date" class="form-control border border-secondary" id="inputDate" >
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputState">Area de Entrega</label>
                                <select id="inputState" class="form-control border border-secondary">
                                    <option selected>Divsion de ingenieria</option>
                                    <option>Turismo</option>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="inputState">Captura de evidencia</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="validatedCustomFile" required>
                                    <label class="custom-file-label  border border-secondary" for="validatedCustomFile">Archivo..</label>
                                </div>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary">Subir Archivo</button>
                        </form>                                       
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Fin de apartado de formulario -->
@endsection
