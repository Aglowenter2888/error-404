<?php

namespace App\Http\Controllers;

use App\Document;
use Illuminate\Http\Request;

class DocumentosController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function search(Request $request){
        $search = $request->get('search'); 
        $file=Document::where('titulo','like',"%$search%")
            ->orWhere('autor','like',"%$search%")
            ->orWhere('descripcion','like',"%$search%")
            ->orWhere('created_at','like',"%$search%")
            ->get();

        return view('documentos.index',compact('file'));
    }

    public function fechaActual(Request $request){
        $fechaActual = $request->get('fechaActual');      
        $file=Document::where('created_at','like',"%$fechaActual%")->get();

        return view('documentos.index',compact('file'));
    }

    public function primerAño(Request $request){
        $pasado = $request->get('pasado');      
        $file=Document::where('created_at','like',"%$pasado%")->get();
        
        return view('documentos.index',compact('file'));
    }

    public function tercerAño(Request $request){
        $antiguo = $request->get('antiguo');      
        $file=Document::where('created_at','like',"%$antiguo%")->get();

        return view('documentos.index',compact('file'));
    }

    public function index(){
        $file=Document::all();

        return view('documentos.index',compact('file'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create(){
        return view('documentos.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request){
        $data = new Document;

        if($request->file('file'))
        {
            $file=$request->file('file');
            $filename=time().'.'.$file->getClientOriginalExtension();
            $request->file->move('storage/' , $filename);

            $data->file=$filename;
        }

        $data->titulo=$request->titulo;
        $data->autor=$request->autor;
        $data->descripcion=$request->descripcion;
        $data->save();

        return redirect()->route('archivos.index');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id){
        $data=Document::find($id);

        return view('documentos.view',compact('data'));
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function download($file){
        return response()->download('storage/' .$file);
    }

    public function edit($id){
    //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id){
    //
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id){
        $data = Document::find($id);
        $data->delete();

        return redirect()->route('archivos.index');
    }
}
